import axios from "axios";

export default class PostService {
  static async getAll() {
    const res = await axios.get('https://hacker-news.firebaseio.com/v0/newstories.json');
    const idList = res.data.slice(0, 10);
    
    const posts = [];
    idList.forEach( async (id) => {
      let p = await this.getById(id);
      posts.push( p );
    });

    return posts;
  }

  static async getById(id) {
    const res = await axios.get(`https://hacker-news.firebaseio.com/v0/item/${id}.json`);
    return res.data
  }

  static async getCommentByPostId(id) {
    const res = await axios.get(`https://jsonplaceholder.typicode.com/posts/${id}/comments` );
    return res
  }

}
