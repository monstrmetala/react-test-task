import React from 'react';
import cs from './MyModal.module.css';


const MyModal = ({children, visible, setVisible}) => {

  const rootClass = [cs.myModal];
  if (visible) {
    rootClass.push(cs.active);
  }

  return (
    <div className={rootClass.join(' ')} onClick={() => setVisible(false)}>
      <div className={cs.myModalContent} onClick={e => e.stopPropagation() }>{children}</div>
    </div>
  )
}

export default MyModal;