import { Link } from "react-router-dom";

const PostItem = (props) => {
  console.log('PostItem props', props);

  return (
    <div className="post">
      <div className="post__content">

        <Link to={`/posts/${props.post.id}`} >
          <strong>{props.post.title}</strong>
        </Link>

      </div>
      <div className='post__btns'>
        <h3 style={ {textAlign: 'right'} }>Score: {props.post.score}</h3>
      </div>
    </div>
  )
}

export default PostItem;