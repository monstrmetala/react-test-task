import PostItem from "./PostItem";

const PostList = ({posts, title, remove}) => {
  console.log('PostList posts', posts)
  console.log('PostList posts.length', posts.length)

  if (!posts.length) {
    return (
      <h2>Posts list is Empty</h2>
    )
  } 

  return (
    <div className="post-list">
      <h1 style={{textAlign: 'center'}}>{title}</h1>
        {
          posts.map((p) => 
            <PostItem key={p.id} post={p} /> 
          )
        }
    </div>
  )
}

export default PostList;