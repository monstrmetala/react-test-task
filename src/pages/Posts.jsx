import React, { useState, useEffect } from 'react';
import PostService from '../API/PostService';
import PostFilter from '../component/PostFilter';
import PostList from '../component/PostList';
import MyButton from '../component/UI/button/MyButton';
import Loader from '../component/UI/Loader/Loader';
import { useFetching } from '../hooks/useFetching';
import { usePosts } from '../hooks/usePost';



function Posts() {
  const [posts, setPosts] = useState([]);
  const [filter, setFilter] = useState({sort: '', query: ''});
  const sortAndSearchPosts = usePosts(posts, filter.sort, filter.query);
  
  const [fetchPosts, isPostLoading, postError] = useFetching( async () => {
    const posts = await PostService.getAll();
    setPosts(posts);
  });


  useEffect( () => {
    fetchPosts();
  }, []);


  return (
    <div className="App">
      <MyButton onClick={fetchPosts} >Get Posts</MyButton>
      <hr/>

      {postError &&
        <h2>Ошибка: <em>{postError}</em></h2>
      }

      {
        isPostLoading
          ? <Loader />
          : <PostList posts={sortAndSearchPosts} title="Список постов" />
      }
      <hr/>
      
    </div>
  );
}

export default Posts;
